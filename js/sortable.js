(function($) {
  Drupal.behaviors.sortable = {
    attach: function(context, settings) {
      jQuery('.sortable', context).once('sortable', function () {

        // Apply sortable plugin.
        var $this = jQuery(this);
        if (!$this.hasClass('disabled')) {
          $this.sortable();

          // Apply settings:
          var wrapper = $this.closest('.sortable-wrapper');
          var field_name = get_field_name(wrapper);

          if (field_name != null && settings.sortable[field_name] != undefined) {
            if (settings.sortable[field_name].disable) {
              $this.sortable('disable').addClass('no-draggable');
            }
            else {
              $this.addClass('draggable');
            }
          }

          // Locate textfield value container.
          var $value_container = $this.siblings('.form-item').find('input.form-text');

          // User can select items. Set a click handler and apply selected class if clicked.
          $this.click(function(e) {
            // Find the right li.
            var elem = jQuery(e.target).closest('li');

            // Set the cardinality:
            if (!elem.hasClass('selected') && settings.sortable[field_name].cardinality >= 1) {
              set_maximun_selected_items($this, settings.sortable[field_name].cardinality - 1);
            }

            elem.toggleClass('selected');
            set_sorted_value($this, $value_container);
          });

          // Listen for the sortupdate event.
          $this.bind('sortupdate', function (e) {
            set_sorted_value($this, $value_container);
          });
        }
      });
    }
  };

  /**
   * Sets maximun selected sortable items.
   * @NOTE: removes items DESC by weight.
   *
   * @param jQueryList $list
   * @param Integer cardinality
   */
  function set_maximun_selected_items($list, max) {
    var $selected = $list.find('li.selected');
    var total     = $selected.length;

    if (total > max) {
      $($selected.get().reverse().slice(0, total - max)).removeClass('selected');
    }
  }

  /**
   * Sets sortable value in the right textfield value container.
   *
   * @param {type} $list
   * @param {type} $input
   * @returns {undefined}
   */
  function set_sorted_value($list, $input) {
    var value_array = [];
    $list.find('li.selected').each(function (item, elem) {
      var $elem = jQuery(elem);
      value_array.push($elem.data('sortable-value'));
    });
    $input.val(value_array.join(', ')).change();
  }

  /**
   * Extracts field_name  from wrapper
   * @param {type} wrapper
   * @returns {String}
   */
  function get_field_name(wrapper) {
    var classes = wrapper.attr("class").split(' ');
    var field_name = null;
    for (var i = 0; i < classes.length; i++) {
      if (classes[i].indexOf('sortable-field_name') >= 0) {
        field_name = classes[i].replace('sortable-field_name-', '');
        break;
      }
    }
    return field_name;
  }

//  function f($list, values) {
//    var values_array = values.slpit(', ');
//
//    $list.find('li').each(function (item, elem) {
//      var $elem = jQuery(elem);
//      if (jQuery.inArray(values_array, $elem.data('sortable-value')) != -1)   {
//        // This elem value is in values array.
//
//      }
//    });
//  }

})(jQuery);
